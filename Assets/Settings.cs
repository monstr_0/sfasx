﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] AudioMixer audioMixer;

    bool fullscreen;
    bool Fullscreen { get { return fullscreen; } 
        set {
            fullscreen = value;
            PlayerPrefs.SetInt("fullscreen", fullscreen ? 1 : 0);
        }
    }

    Vector2Int windowSize;
    Vector2Int WindowSize { get { return windowSize; }
        set {
            windowSize = value;
            PlayerPrefs.SetInt("windowSize.x", windowSize.x);
            PlayerPrefs.SetInt("windowSize.y", windowSize.y);
        }
    }

    int musicVolume;
    int MusicVolume {
        get { return musicVolume; }
        set {
            musicVolume = value;
            PlayerPrefs.SetInt("musicVolume", musicVolume);
        }
    }

    int sfxVolume;
    int SfxVolume {
        get { return sfxVolume; }
        set {
            sfxVolume = value;
            PlayerPrefs.SetInt("sfxVolume", sfxVolume);
        }
    }

    [Space]
    [SerializeField] Toggle fullscreenToggle;

    [SerializeField] TMP_Dropdown resolutionDropdown;
    List<Resolution> resolutions;

    [SerializeField] Slider musicSlider;
    [SerializeField] Slider sfxSlider;
    

    // Start is called before the first frame update
    void Start()
    {
        Fullscreen = (PlayerPrefs.GetInt("fullscreen", 1) == 1);
        windowSize.x = PlayerPrefs.GetInt("windowSize.x", Screen.width);
        windowSize.y = PlayerPrefs.GetInt("windowSize.y", Screen.height);
        musicVolume = PlayerPrefs.GetInt("musicVolume", 100);
        sfxVolume = PlayerPrefs.GetInt("sfxVolume", 100);

        SetupFullscreen();
        SetupResolutions();
        SetupMusicVolume();
        SetupSfxVolume();
    }


    void SetupFullscreen()
    {
#if !UNITY_WEBGL
        fullscreenToggle.SetIsOnWithoutNotify(fullscreen);
#else
        fullscreenToggle.gameObject.SetActive(false);
#endif
    }

    void SetupResolutions()
    {
#if !UNITY_WEBGL
        Screen.SetResolution(WindowSize.x, WindowSize.y, Fullscreen ? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed);

        List<string> optionStrings = new List<string>();
        resolutions = new List<Resolution>();
        Resolution[] allResolutions = Screen.resolutions;
        for (int i = 0; i < allResolutions.Length; i++)
        {
            Resolution res = allResolutions[i];

            res.refreshRate = -1;
            if (resolutions.Contains(res)) continue;
            else resolutions.Add(res);

        }

        int current = 0;
        for (int i = 0; i < resolutions.Count; i++)
        {
            string newData = $"{resolutions[i].width}x{resolutions[i].height}";
            optionStrings.Add(newData);
            if (resolutions[i].width == WindowSize.x && resolutions[i].height == WindowSize.y) current = optionStrings.Count - 1;
        }

        resolutionDropdown.ClearOptions();
        resolutionDropdown.AddOptions(optionStrings);
        resolutionDropdown.SetValueWithoutNotify(current);
#else
        resolutionDropdown.gameObject.SetActive(false);
#endif
    }

    void SetupMusicVolume()
    {
        audioMixer.SetFloat("MusicVolume", VolumeToMixerValue(MusicVolume));

        musicSlider.minValue = 0;
        musicSlider.maxValue = 100;
        musicSlider.wholeNumbers = true;
        musicSlider.SetValueWithoutNotify(MusicVolume);
    }

    void SetupSfxVolume()
    {
        audioMixer.SetFloat("SfxVolume", VolumeToMixerValue(SfxVolume));

        sfxSlider.minValue = 0;
        sfxSlider.maxValue = 100;
        sfxSlider.wholeNumbers = true;
        sfxSlider.SetValueWithoutNotify(SfxVolume);
    }

    float VolumeToMixerValue(float volume)
    {
        float result;

        result = -80f + Mathf.Sqrt(volume / 100f) * 80f;

        return result;
    }

    public void UpdateFullscreen()
    {
#if !UNITY_WEBGL
        Fullscreen = fullscreenToggle.isOn;

        Screen.SetResolution(Screen.width, Screen.height, Fullscreen ? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed);
#endif
    }

    public void UpdateResolution()
    {
#if !UNITY_WEBGL
        Resolution resolution = resolutions[resolutionDropdown.value];
        WindowSize = new Vector2Int(resolution.width, resolution.height);

        Screen.SetResolution(WindowSize.x, WindowSize.y, Fullscreen ? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed);
#endif
    }

    public void UpdateVolume()
    {
        MusicVolume = Mathf.RoundToInt(musicSlider.value);
        SfxVolume = Mathf.RoundToInt(sfxSlider.value);

        audioMixer.SetFloat("MusicVolume", VolumeToMixerValue(MusicVolume));
        audioMixer.SetFloat("SfxVolume", VolumeToMixerValue(SfxVolume));
    }
}
