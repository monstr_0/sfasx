﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    [SerializeField] private List<EnvironmentTile> AccessibleTiles;
    [SerializeField] private List<EnvironmentTile> InaccessibleTiles;
    [SerializeField] private EnvironmentTile fenceNorthTile;
    [SerializeField] private EnvironmentTile fenceSouthTile;
    [SerializeField] private EnvironmentTile fenceEastTile;
    [SerializeField] private EnvironmentTile fenceWestTile;

    [SerializeField] private Vector2Int Size;
    [SerializeField] private float AccessiblePercentage;

    private EnvironmentTile[][] mMap;
    private List<EnvironmentTile> mAll;
    private List<EnvironmentTile> mToBeTested;
    private List<EnvironmentTile> mLastSolution;

    private readonly Vector3 NodeSize = Vector3.one * 9.0f; 
    private const float TileSize = 10.0f;
    private const float TileHeight = 2.5f;

    public EnvironmentTile Start { get; private set; }

    private void Awake()
    {
        mAll = new List<EnvironmentTile>();
        mToBeTested = new List<EnvironmentTile>();
    }

    private void OnDrawGizmos()
    {
        // Draw the environment nodes and connections if we have them
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    if (mMap[x][y].Connections != null)
                    {
                        for (int n = 0; n < mMap[x][y].Connections.Count; ++n)
                        {
                            Gizmos.color = Color.blue;
                            Gizmos.DrawLine(mMap[x][y].Position, mMap[x][y].Connections[n].Position);
                        }
                    }

                    // Use different colours to represent the state of the nodes
                    Color c = Color.white;
                    if ( !mMap[x][y].IsAccessible )
                    {
                        c = Color.red;
                    }
                    else
                    {
                        if(mLastSolution != null && mLastSolution.Contains( mMap[x][y] ))
                        {
                            c = Color.green;
                        }
                        else if (mMap[x][y].Visited)
                        {
                            c = Color.yellow;
                        }
                    }

                    Gizmos.color = c;
                    Gizmos.DrawWireCube(mMap[x][y].Position, NodeSize);
                }
            }
        }
    }

    private void Generate()
    {
        // Setup the map of the environment tiles according to the specified width and height
        // Generate tiles from the list of accessible and inaccessible prefabs using a random
        // and the specified accessible percentage

        //Assume a size slightly larger than Size to account for the fences
        Vector2Int sizeWithFences = new Vector2Int(Size.x + 2, Size.y + 2);

        mMap = new EnvironmentTile[sizeWithFences.x][];

        int halfWidth = sizeWithFences.x / 2;
        int halfHeight = sizeWithFences.y / 2;
        Vector3 position = new Vector3( -(halfWidth * TileSize), 0.0f, -(halfHeight * TileSize) );

        mAll = new List<EnvironmentTile>();
        Game.OrcSpawns = new List<EnvironmentTile>();

        for ( int x = 0; x < sizeWithFences.x; ++x)
        {
            mMap[x] = new EnvironmentTile[sizeWithFences.y];
            for ( int y = 0; y < sizeWithFences.y; ++y)
            {
                //The starting tile is in the centre
                bool start = (x == Mathf.FloorToInt((sizeWithFences.x) / 2f) && y == Mathf.FloorToInt((sizeWithFences.y) / 2f));
                //Orcs spawn in the corners
                bool orcSpawn = (x == 1 && y == 1) || (x == sizeWithFences.x - 2 && y == 1) || (x == sizeWithFences.x - 2 && y == sizeWithFences.y - 2) || (x == 1 && y == sizeWithFences.y - 2);
                //Fences are on the outside
                bool isFence = (x <= 0 || x >= sizeWithFences.x - 1 || y <= 0 || y >= sizeWithFences.y - 1);

                bool isAccessible;
                EnvironmentTile prefab;
                if (isFence)
                {
                    isAccessible = false;
                    if (x <= 0 && y > 0 && y < sizeWithFences.y - 1) prefab = fenceWestTile;
                    else if (x >= sizeWithFences.x - 1 && y > 0 && y < sizeWithFences.y - 1) prefab = fenceEastTile;
                    else if (x > 0 && x < sizeWithFences.x - 1 && y <= 0) prefab = fenceNorthTile;
                    else if (x > 0 && x < sizeWithFences.x - 1 && y >= sizeWithFences.y - 1) prefab = fenceSouthTile;
                    else prefab = AccessibleTiles[Random.Range(0, AccessibleTiles.Count)];
                }
                else
                {
                    isAccessible = start || orcSpawn || Random.value < AccessiblePercentage;
                    List<EnvironmentTile> tiles = isAccessible ? AccessibleTiles : InaccessibleTiles;
                    prefab = tiles[Random.Range(0, tiles.Count)];
                }

                EnvironmentTile tile = Instantiate(prefab, position, Quaternion.identity, transform);
                tile.Position = new Vector3( position.x + (TileSize / 2), TileHeight, position.z + (TileSize / 2));
                tile.IsAccessible = isAccessible;
                tile.gameObject.name = string.Format("Tile({0},{1})", x, y);
                tile.SnowTurns = (isAccessible && !isFence) ? 0 : -1;
                tile.IsScenery = (!isAccessible && !isFence);
                mMap[x][y] = tile;
                mAll.Add(tile);

                if (x == 0 && y == 0) CameraControl.TopLeft = tile.Position;
                else if (x == sizeWithFences.x - 1 && y == sizeWithFences.y - 1) CameraControl.BottomRight = tile.Position;

                if (start && !isFence)
                {
                    Start = tile;
                }
                
                if (orcSpawn && !isFence)
                {
                    Game.OrcSpawns.Add(tile);
                }

                position.z += TileSize;
            }

            position.x += TileSize;
            position.z = -(halfHeight * TileSize);
        }
    }

    private void SetupConnections()
    {
        // Currently we are only setting up connections between adjacent nodes
        for (int x = 0; x < mMap.Length; ++x)
        {
            for (int y = 0; y < mMap[0].Length; ++y)
            {
                EnvironmentTile tile = mMap[x][y];
                tile.Connections = new List<EnvironmentTile>();
                if (x > 0)
                {
                    tile.Connections.Add(mMap[x - 1][y]);
                }

                if (x < mMap.Length - 1)
                {
                    tile.Connections.Add(mMap[x + 1][y]);
                }

                if (y > 0)
                {
                    tile.Connections.Add(mMap[x][y - 1]);
                }

                if (y < mMap[0].Length - 1)
                {
                    tile.Connections.Add(mMap[x][y + 1]);
                }
            }
        }
    }

    private float Distance(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the length of the connection between these two nodes to find the distance, this 
        // is used to calculate the local goal during the search for a path to a location
        float result = float.MaxValue;
        EnvironmentTile directConnection = a.Connections.Find(c => c == b);
        if (directConnection != null)
        {
            result = TileSize;
        }
        return result;
    }

    private float Heuristic(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the locations of the node to estimate how close they are by line of sight
        // experiment here with better ways of estimating the distance. This is used to
        // calculate the global goal and work out the best order to prossess nodes in
        return Vector3.Distance(a.Position, b.Position);
    }

    public void GenerateWorld()
    {
        CleanUpWorld();
        Generate();
        SetupConnections();
    }

    public void CleanUpWorld()
    {
        //It's safer to just delete all EnvironmentTiles.
        //Since there's only 1 Environment in the scene they should all belong to the same Environment anyway.
        EnvironmentTile[] existingTiles = FindObjectsOfType<EnvironmentTile>();
        for (int i = 0; i < existingTiles.Length; i++)
        {
            Destroy(existingTiles[i].gameObject);
        }
        /*
        if (mMap != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    if (mMap[x][y] != null) Destroy(mMap[x][y].gameObject);
                }
            }
        }
        */
    }

    public void ReduceTileSnow(int amount = 1)
    {
        for (int i = 0; i < mAll.Count; i++)
        {
            if (mAll[i].SnowTurns > 0) mAll[i].SnowTurns = Mathf.Max(mAll[i].SnowTurns - amount, 0);
        }
    }

    public void MakeAccessibleTilesSnow()
    {
        for (int i = 0; i < mAll.Count; i++)
        {
            if (mAll[i].IsAccessible) mAll[i].SnowTurns = 0;
        }
    }

    public List<EnvironmentTile> Solve(EnvironmentTile begin, EnvironmentTile destination, bool includeNonAccessible = false)
    {
        List<EnvironmentTile> result = null;
        if (begin != null && destination != null)
        {
            // Nothing to solve if there is a direct connection between these two locations
            if (begin.Connections == null) Debug.LogWarning("NULL connections");
            EnvironmentTile directConnection = begin.Connections.Find(c => c == destination);
            if (directConnection == null)
            {
                // Set all the state to its starting values
                mToBeTested.Clear();

                for( int count = 0; count < mAll.Count; ++count )
                {
                    mAll[count].Parent = null;
                    mAll[count].Global = float.MaxValue;
                    mAll[count].Local = float.MaxValue;
                    mAll[count].Visited = false;
                }

                // Setup the start node to be zero away from start and estimate distance to target
                EnvironmentTile currentNode = begin;
                currentNode.Local = 0.0f;
                currentNode.Global = Heuristic(begin, destination);

                // Maintain a list of nodes to be tested and begin with the start node, keep going
                // as long as we still have nodes to test and we haven't reached the destination
                mToBeTested.Add(currentNode);

                while (mToBeTested.Count > 0 && currentNode != destination)
                {
                    // Begin by sorting the list each time by the heuristic
                    mToBeTested.Sort((a, b) => (int)(a.Global - b.Global));

                    // Remove any tiles that have already been visited
                    mToBeTested.RemoveAll(n => n.Visited);

                    // Check that we still have locations to visit
                    if (mToBeTested.Count > 0)
                    {
                        // Mark this note visited and then process it
                        currentNode = mToBeTested[0];
                        currentNode.Visited = true;

                        // Check each neighbour, if it is accessible and hasn't already been 
                        // processed then add it to the list to be tested 
                        for (int count = 0; count < currentNode.Connections.Count; ++count)
                        {
                            EnvironmentTile neighbour = currentNode.Connections[count];

                            if (!neighbour.Visited && (neighbour.IsAccessible || includeNonAccessible))
                            {
                                mToBeTested.Add(neighbour);
                            }

                            // Calculate the local goal of this location from our current location and 
                            // test if it is lower than the local goal it currently holds, if so then
                            // we can update it to be owned by the current node instead 
                            float possibleLocalGoal = currentNode.Local + Distance(currentNode, neighbour);
                            if (possibleLocalGoal < neighbour.Local)
                            {
                                neighbour.Parent = currentNode;
                                neighbour.Local = possibleLocalGoal;
                                neighbour.Global = neighbour.Local + Heuristic(neighbour, destination);
                            }
                        }
                    }
                }

                // Build path if we found one, by checking if the destination was visited, if so then 
                // we have a solution, trace it back through the parents and return the reverse route
                if (destination.Visited)
                {
                    result = new List<EnvironmentTile>();
                    EnvironmentTile routeNode = destination;

                    while (routeNode.Parent != null)
                    {
                        result.Add(routeNode);
                        routeNode = routeNode.Parent;
                    }
                    result.Add(routeNode);
                    result.Reverse();

                    Debug.LogFormat("Path Found: {0} steps {1} long", result.Count, destination.Local);
                }
                else
                {
                    Debug.LogWarning("Path Not Found");
                }
            }
            else
            {
                if (directConnection.IsAccessible || includeNonAccessible)
                {
                    result = new List<EnvironmentTile>();
                    result.Add(begin);
                    result.Add(destination);
                    Debug.LogFormat("Direct Connection: {0} <-> {1} {2} long", begin, destination, TileSize);
                }
            }
        }
        else
        {
            Debug.LogWarning("Cannot find path for invalid nodes");
        }

        mLastSolution = result;

        return result;
    }
}
