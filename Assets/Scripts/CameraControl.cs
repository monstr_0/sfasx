﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public static Vector3 TopLeft { get; set; } //The position that will be in the top left of the screen
    public static Vector3 BottomRight { get; set; } //The position that will be in the bottom right of the screen
    public static Vector3 Centre { get { return (TopLeft + BottomRight) / 2f; } }

    [SerializeField] float zoom = 1f;
    [SerializeField] Vector2 position;

    public static Camera MainCamera { get; private set; }

    float lastClickTime = 0f;

    void Start()
    {
        MainCamera = GetComponent<Camera>();
    }

    void Update()
    {
        zoom -= Input.mouseScrollDelta.y * Time.deltaTime;
        zoom = Mathf.Clamp(zoom, 0.1f, 1f);
        
        if (Input.GetMouseButton(1) || Input.GetMouseButton(2))
        {
            position.x -= Input.GetAxisRaw("Mouse X") * zoom * (MainCamera.fieldOfView / 2f);
            position.y -= Input.GetAxisRaw("Mouse Y") * zoom * (MainCamera.fieldOfView / 2f);
        }

        lastClickTime += Time.unscaledDeltaTime;
        if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            if (lastClickTime < 0.5f) //click was a double click
            {
                zoom = 1f;
                position = Vector2.zero;
            }
            lastClickTime = 0f;
        }

        Vector2 size = Vector3.zero;
        size.x = Mathf.Abs(BottomRight.x - TopLeft.x);
        size.y = Mathf.Abs(BottomRight.z - TopLeft.z);

        //clamp so that the camera doesn't wonder off too far
        position.x = Mathf.Clamp(position.x, -size.x / 2, size.x / 2);
        position.y = Mathf.Clamp(position.y, -size.y / 2, size.y / 2);

        float multiplyDistanceToAccountForUI = 1.25f;
        float distance = multiplyDistanceToAccountForUI * Mathf.Abs(TopLeft.z - BottomRight.z) / (MainCamera.fieldOfView / 45f); //distance from the centre at zoom 1.0f

        Vector3 targetPosition = Centre + new Vector3(position.x, 0f, position.y) - transform.forward * zoom * distance;

        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime / 0.1f);
    }
}
