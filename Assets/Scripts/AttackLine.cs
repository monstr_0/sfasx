﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackLine : MonoBehaviour
{
    static AttackLine instance;

    LineRenderer lineRenderer;

    public static EnvironmentTile StartTile { get; private set; }
    public static EnvironmentTile EndTile { get; private set; }

    public static bool Visible { get { return instance != null ? instance.lineRenderer.enabled : false; } set { if (instance != null) instance.lineRenderer.enabled = value; } }

    void Start()
    {
        instance = this;
        lineRenderer = GetComponent<LineRenderer>();
    }

    void RedrawLine()
    {
        Vector3 midPoint = (StartTile.Position + EndTile.Position) / 2f + Vector3.up * 10f;
        Vector3[] curvePoints = CalcBezier(StartTile.Position + Vector3.up * 3f, midPoint, EndTile.Position + Vector3.up * 3f);
        lineRenderer.positionCount = curvePoints.Length;
        lineRenderer.SetPositions(curvePoints);
    }

    //3 point Bezier Curve
    Vector3[] CalcBezier(Vector3 start, Vector3 mid, Vector3 end, int numPoints = 8)
    {
        Vector3[] points = new Vector3[numPoints];
        points[0] = start;
        points[numPoints - 1] = end;
        for (int i = 1; i < numPoints; i++)
        {
            float percent = (float)i / numPoints;
            Vector3 lineStart = Vector3.Lerp(start, mid, percent);
            Vector3 lineEnd = Vector3.Lerp(mid, end, percent);
            points[i] = Vector3.Lerp(lineStart, lineEnd, percent);
        }

        return points;
    }

    public static void SetStartEndTiles(EnvironmentTile startTile, EnvironmentTile endTile)
    {
        StartTile = startTile;
        EndTile = endTile;
        if (instance != null) instance.RedrawLine();
    }
}
