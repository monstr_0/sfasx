﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AlwaysPointUp : MonoBehaviour
{
    void Update()
    {
        transform.up = Vector3.up;
    }
}
