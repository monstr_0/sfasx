﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcVoiceContainer : MonoBehaviour
{
    public static OrcVoiceContainer instance;

    [SerializeField] AudioClip[] voices;
    public static AudioClip[] Voices { get {
            if (instance != null) return instance.voices;
            else return new AudioClip[0];
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public static AudioClip GetRandomClip()
    {
        if (instance == null) return null;

        return Voices[Random.Range(0, Voices.Length)];
    }
}
