﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ActionUI : MonoBehaviour
{
    [SerializeField] Image backgroundImage;
    [SerializeField] Image actionImage;
    [SerializeField] Image nextArrowImage;
    [Space]
    public Sprite sprite;
    public Color color;
    public bool isLast = false;
    public bool show = false;

    // Update is called once per frame
    void Update()
    {
        backgroundImage.enabled = show;
        
        nextArrowImage.enabled = !isLast && show;

        actionImage.sprite = sprite;
        actionImage.color = color;
        actionImage.enabled = show;
    }
}
