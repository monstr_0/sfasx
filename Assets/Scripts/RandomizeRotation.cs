﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeRotation : MonoBehaviour
{
    [SerializeField] float offset = 45f;
    [SerializeField] int increments = 4;

    void Start()
    {
        float deg = 360f / (float)increments;
        transform.rotation = Quaternion.Euler(0f, offset + Random.Range(0, increments) * deg, 0f);
    }
}
