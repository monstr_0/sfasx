﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public static float MovementMultiplier { get; set; } //Value to multiply movement and animations by. Less is faster.

    [SerializeField] private float SingleNodeMoveTime = 0.5f;
    [SerializeField] private int maxActionPoints = 2;
    [SerializeField] private int movementPerAction = 4;
    [SerializeField] private int attackRange = 4;

    public int AttackRange { get { return attackRange; } }

    public int MaxActionPoints { get { return maxActionPoints; } }
    public int ActionPoints { get; private set; }

    public EnvironmentTile CurrentPosition { get; set; }

    public bool IsRunning { get; private set; }

    Character currentAttack;
    public Character CurrentAttack { get { return currentAttack; }
        set {
            currentAttack = value;

            if (currentAttack != null)
            {
                //Show the attack in the UI
                AttackLine.SetStartEndTiles((CurrentPath == null) ? CurrentPosition : CurrentPath[CurrentPath.Count - 1], currentAttack.CurrentPosition);
                AttackLine.Visible = true;
            } 

            if (currentAttack == null) AttackLine.Visible = false;
        } 
    }

    [Space]
    [SerializeField] bool isOnFire = false;
    public bool IsOnFire { get { return isOnFire; } }

    private void OnDestroy()
    {
        if (CurrentPosition != null) CurrentPosition.IsAccessible = true;
    }

    List<EnvironmentTile> currentPath;
    public List<EnvironmentTile> CurrentPath { get { return currentPath; } 
        set {
            if (currentPath != null)
            {
                //Reset path UI
                for (int i = 0; i < currentPath.Count; i++)
                {
                    currentPath[i].IsPath = false;
                    if (i == currentPath.Count - 1) currentPath[i].IsDestination = false;
                }
            }
            
            currentPath = value;

            ActionPoints = maxActionPoints;
            if (currentPath != null)
            {
                //Calculate action points used with this path
                int maxMoves = movementPerAction * maxActionPoints + 1;
                if (currentPath.Count > maxMoves) currentPath = currentPath.GetRange(0, maxMoves);
                float percent = (float)(currentPath.Count - 1) / (maxMoves - 1);
                ActionPoints -= Mathf.CeilToInt((float)maxActionPoints * percent);

                //Show the path in the UI
                for (int i = 0; i < currentPath.Count; i++)
                {
                    currentPath[i].IsPath = true;
                    if (i == currentPath.Count - 1) currentPath[i].IsDestination = true;
                }
            }
        }
    }

    public Animator Animator { get; private set; }
    private int AnimatorMoveSpeed = Animator.StringToHash("MoveSpeed");
    private int AnimatorTimeScale = Animator.StringToHash("TimeScale");

    [Space]
    public AudioClip[] DeathSounds;
    public AudioClip[] MoveSounds;
    public AudioClip[] AttackSounds;

    public AudioSource AudioSource { get; private set; }

    private void Start()
    {
        Animator = GetComponent<Animator>();
        AudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (MovementMultiplier > 0) Animator.SetFloat(AnimatorTimeScale, 1f / MovementMultiplier);
    }

    public enum VoiceTypes
    {
        DEATH,
        MOVE,
        ATTACK
    }

    public void PlayVoice(VoiceTypes voiceType)
    {
        switch (voiceType)
        {
            case VoiceTypes.DEATH:
                AudioSource.clip = DeathSounds[Random.Range(0, DeathSounds.Length)];
                AudioSource.Play();
                break;
            case VoiceTypes.MOVE:
                AudioSource.clip = MoveSounds[Random.Range(0, MoveSounds.Length)];
                AudioSource.Play();
                break;
            case VoiceTypes.ATTACK:
                AudioSource.clip = AttackSounds[Random.Range(0, AttackSounds.Length)];
                AudioSource.Play();
                break;
        }
    }

    private IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.rotation = Quaternion.LookRotation(destination - position, Vector3.up);

            Vector3 p = transform.position;
            float t = 0.0f;

            float movementTime = SingleNodeMoveTime;
            if (movementTime > 0) Animator.SetFloat(AnimatorMoveSpeed, 1f / movementTime);
            while (t < movementTime)
            {
                if (MovementMultiplier > 0) t += Time.deltaTime / MovementMultiplier;
                else t = movementTime;
                p = Vector3.Lerp(position, destination, t / movementTime);
                transform.position = p;
                yield return null;
            }
        }
    }

    private IEnumerator DoGoTo(List<EnvironmentTile> route)
    {
        // Move through each tile in the given route
        if (route != null)
        {
            Vector3 position = CurrentPosition.Position;
            for (int count = 0; count < route.Count; ++count)
            {
                CurrentPosition.IsPath = false;
                CurrentPosition.IsDestination = false;
                Vector3 next = route[count].Position;
                yield return DoMove(position, next);
                CurrentPosition = route[count];
                if (isOnFire)
                {
                    CurrentPosition.SnowTurns = -1;
                }
                position = next;
            }

            CurrentPosition.IsPath = false;
            CurrentPosition.IsDestination = false;
            CurrentPath = null;
        }

        IsRunning = false;
        Animator.SetFloat(AnimatorMoveSpeed, 0);
    }

    public void GoTo(List<EnvironmentTile> route)
    {
        // Clear all coroutines before starting the new route so 
        // that clicks can interupt any current route animation
        StopAllCoroutines();
        IsRunning = true;
        StartCoroutine(DoGoTo(route));
    }

    public void Run()
    {
        if (CurrentPath != null && CurrentPath.Count > 0) GoTo(CurrentPath);
    }
}
