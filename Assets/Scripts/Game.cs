﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Character Character;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas SettingsMenu;
    [SerializeField] private Canvas HelpMenu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Canvas GameOverScreen;
    [SerializeField] private Transform CharacterStart;
    [SerializeField] private Character Orc;
    [SerializeField] private Character FireOrc;
    [Space]
    [SerializeField] private OneShotSound ButtonClickSound;
    [SerializeField] private OneShotSound UnavailableSound;
    [SerializeField] private OneShotSound ActionSound;
    [Space]
    [SerializeField] private Animator PlayPauseAnimator;
    private int PlayPauseAnimatorPlaying = Animator.StringToHash("Playing");
    private int PlayPauseAnimatorFastForward = Animator.StringToHash("FastForward");
    [SerializeField] private TextMeshProUGUI TurnCounterTextMesh;
    [SerializeField] private TextMeshProUGUI ActionCounterTextMesh;
    [SerializeField] private TextMeshProUGUI SurvivalMessageTextMesh;

    public static Character MainCharacter { get; private set; }

    public static List<EnvironmentTile> OrcSpawns { get; set; }

    private RaycastHit[] mRaycastHits;
    private List<Character> mOrcs;
    private Environment mMap;

    private readonly int NumberOfRaycastHits = 1;

    EnvironmentTile targetingTile;

    int nextOrcTurn = 1;
    int orcsToSpawn = 1; //In case we want to spawn more orcs next spawn

    bool canTakeTurn = true;
    float waitBeforeCanSkipTurn = 0f; //To try and prevent accidental turn skips when spamming fast-forward

    public int TurnCount { get; private set; }

    private int AnimatorCharacterNormal = Animator.StringToHash("CharacterNormal");
    private int AnimatorCharacterDieToOrc = Animator.StringToHash("CharacterDieToOrc");
    private int AnimatorCharacterOrcAttack = Animator.StringToHash("CharacterOrcAttack");
    private int AnimatorCharacterThrowSnowball = Animator.StringToHash("CharacterThrowSnowball");
    private int AnimatorCharacterDieToSnowball = Animator.StringToHash("CharacterDieToSnowball");

    void ResetVariables()
    {
        if (MainCharacter != null) Destroy(MainCharacter.gameObject);
        if (mOrcs != null)
        {
            for (int i = 0; i < mOrcs.Count; i++)
            {
                Destroy(mOrcs[i].gameObject);
            }
        }

        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<Environment>();
        MainCharacter = Instantiate(Character, transform);
        mOrcs = new List<Character>();

        orcsToSpawn = 1;
        nextOrcTurn = 1;
        canTakeTurn = true;

        TurnCount = 0;
        TurnCounterTextMesh.text = $"Turn [{TurnCount}]";
    }

    void Start()
    {
        ResetVariables();

        ShowMenu(true);
        
    }

    private void Update()
    {
        //Check what tile the mouse is hovering over so that we can give feedback to the player about which tile they will be clicking
        Ray mouseRay = MainCamera.ScreenPointToRay(Input.mousePosition);
        int hits = Physics.RaycastNonAlloc(mouseRay, mRaycastHits);
        if (hits > 0)
        {
            EnvironmentTile tile = mRaycastHits[0].transform.GetComponent<EnvironmentTile>();
            if (tile != null)
            {
                if (tile != targetingTile)
                {
                    if (targetingTile != null) targetingTile.IsHovering = false;
                    tile.IsHovering = true;
                    targetingTile = tile;
                }
            }
            else
            {
                if (targetingTile != null)
                {
                    targetingTile.IsHovering = false;
                    targetingTile = null;
                }
            }
        }
        else
        {
            if (targetingTile != null)
            {
                targetingTile.IsHovering = false;
                targetingTile = null;
            }
        }

        if (canTakeTurn)
        {
            if (waitBeforeCanSkipTurn > 0f) waitBeforeCanSkipTurn -= Time.deltaTime;

            if (Input.GetMouseButtonDown(0))
            {
                if (targetingTile != null)
                {
                    MainCharacter.CurrentAttack = null;
                    if (targetingTile == MainCharacter.CurrentPosition)
                    {
                        //cancel actions
                        MainCharacter.CurrentPath = null;
                        MainCharacter.CurrentAttack = null;
                        ActionBarUI.CleanActions();
                    }
                    else
                    {
                        //check if we're selecting an orc to attack
                        for (int i = 0; i < mOrcs.Count; i++)
                        {
                            if (mOrcs[i].CurrentPosition == targetingTile)
                            {
                                MainCharacter.CurrentAttack = mOrcs[i];
                                break;
                            }
                        }

                        //if this is a movement request
                        if (MainCharacter.CurrentAttack == null)
                        {
                            List<EnvironmentTile> route = mMap.Solve(MainCharacter.CurrentPosition, targetingTile);
                            MainCharacter.CurrentPath = route;

                            int actionsTaken = MainCharacter.MaxActionPoints - MainCharacter.ActionPoints;
                            for (int i = 0; i < ActionBarUI.Actions.Length; i++)
                            {
                                if (i < actionsTaken)
                                {
                                    ActionBarUI.Actions[i] = ActionBarUI.ActionTypes.MOVEMENT;
                                }
                                else if (MainCharacter.ActionPoints > 0 && i < MainCharacter.MaxActionPoints)
                                {
                                    ActionBarUI.Actions[i] = ActionBarUI.ActionTypes.SKIP;
                                }
                                else
                                {
                                    ActionBarUI.Actions[i] = ActionBarUI.ActionTypes.NONE;
                                }
                            }

                            if (MainCharacter.CurrentPath != null) Instantiate((MainCharacter.CurrentPath != null) ? ActionSound :  UnavailableSound);
                            ActionCounterTextMesh.text = $"Actions: {MainCharacter.MaxActionPoints - MainCharacter.ActionPoints}/{MainCharacter.MaxActionPoints}";
                            ActionBarUI.UpdateActions();
                        }
                        else //this is an attack request
                        {
                            int actionsTaken = MainCharacter.MaxActionPoints - MainCharacter.ActionPoints;
                            EnvironmentTile attackingFromTile = (MainCharacter.CurrentPath == null) ? MainCharacter.CurrentPosition : MainCharacter.CurrentPath[MainCharacter.CurrentPath.Count - 1];
                            if (attackingFromTile.IsSnow && MainCharacter.ActionPoints > 0)
                            {
                                List<EnvironmentTile> attackPath = mMap.Solve(attackingFromTile, MainCharacter.CurrentAttack.CurrentPosition, true);
                                if (attackPath == null || attackPath.Count - 1 > MainCharacter.AttackRange)
                                {
                                    MainCharacter.CurrentAttack = null;
                                    Instantiate(UnavailableSound);
                                }
                                else
                                {
                                    OneShotSound sound = Instantiate(ActionSound);
                                    sound.GetComponent<AudioSource>().pitch = 0.5f;
                                    ActionBarUI.Actions[actionsTaken] = ActionBarUI.ActionTypes.ATTACK;
                                    ActionCounterTextMesh.text = $"Actions: {MainCharacter.MaxActionPoints - (MainCharacter.ActionPoints - 1)}/{MainCharacter.MaxActionPoints}";
                                    ActionBarUI.UpdateActions();
                                }
                            }
                            else
                            {
                                MainCharacter.CurrentAttack = null;
                                Instantiate(UnavailableSound);
                            }
                        }
                    }
                }
            }

            if (waitBeforeCanSkipTurn <= 0f)
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                {
                    TakeTurn();
                    waitBeforeCanSkipTurn = 1f;
                }
            }
            
        }
        else
        {
            if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
            {
                Character.MovementMultiplier = 0.25f;
                PlayPauseAnimator.SetBool(PlayPauseAnimatorFastForward, true);
            }
            else
            {
                Input.ResetInputAxes();
                Character.MovementMultiplier = 1f;
                PlayPauseAnimator.SetBool(PlayPauseAnimatorFastForward, false);
            }
        }
    }

    public void NextTurnButton()
    {
        if (canTakeTurn)
        {
            TakeTurn();
        }
    }

    void TakeTurn()
    {
        Input.ResetInputAxes();
        StartCoroutine(ExecuteTurn());
    }

    IEnumerator ExecuteTurn()
    {
        #region Player's turn
        MainCharacter.PlayVoice(Character.VoiceTypes.MOVE);
        MainCharacter.Run();

        canTakeTurn = false;
        PlayPauseAnimator.SetBool(PlayPauseAnimatorPlaying, true);

        while (MainCharacter.IsRunning)
        {
            yield return null;
        }

        if (MainCharacter.CurrentAttack != null)
        {
            AttackLine.Visible = false;
            MainCharacter.CurrentPosition.SnowTurns = -1;
            MainCharacter.transform.rotation = Quaternion.LookRotation(MainCharacter.CurrentAttack.CurrentPosition.Position - MainCharacter.CurrentPosition.Position, Vector3.up);
            MainCharacter.CurrentAttack.transform.rotation = Quaternion.LookRotation(MainCharacter.CurrentPosition.Position - MainCharacter.CurrentAttack.CurrentPosition.Position, Vector3.up);

            //Player throws snowball
            MainCharacter.PlayVoice(Character.VoiceTypes.ATTACK);
            MainCharacter.Animator.Play(AnimatorCharacterThrowSnowball);
            while (MainCharacter.Animator.GetCurrentAnimatorStateInfo(0).shortNameHash != AnimatorCharacterThrowSnowball)
            {
                yield return null;
            }
            while (MainCharacter.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
            {
                yield return null;
            }

            //Afterwards, Orc gets hit
            MainCharacter.CurrentAttack.PlayVoice(Character.VoiceTypes.DEATH);
            MainCharacter.CurrentAttack.Animator.Play(AnimatorCharacterDieToSnowball);
            while (MainCharacter.CurrentAttack.Animator.GetCurrentAnimatorStateInfo(0).shortNameHash != AnimatorCharacterDieToSnowball)
            {
                yield return null;
            }
            while (MainCharacter.CurrentAttack.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
            {
                yield return null;
            }

            MainCharacter.CurrentAttack.CurrentPosition.IsAccessible = true;
            mOrcs.Remove(MainCharacter.CurrentAttack);
            Destroy(MainCharacter.CurrentAttack.gameObject);

            MainCharacter.Animator.Play(AnimatorCharacterNormal);
        }
        #endregion

        #region Orc's turn
        for (int i = 0; i < mOrcs.Count; i++)
        {
            mOrcs[i].CurrentPosition.IsAccessible = true;
            List<EnvironmentTile> route = mMap.Solve(mOrcs[i].CurrentPosition, MainCharacter.CurrentPosition);

            //if no path to player was found (enemy is probably blocked off), move towards player and remove the tile that blocks the path
            if (route == null)
            {
                route = mMap.Solve(mOrcs[i].CurrentPosition, MainCharacter.CurrentPosition, true);
                if (route == null || route.Count < 1)
                {
                    Debug.LogError("No Route including inaccessible was found!");
                    mOrcs[i].CurrentPosition.IsAccessible = false;
                    continue;
                }

                if (route.Count > 3) route = route.GetRange(0, 2);
                if (route[1].IsScenery)
                {
                    Debug.Log($"Removing Tile: {route[1].gameObject.name}");
                    yield return route[1].ClearOfScenery();
                }
            }

            //Make sure the end of the path is the tile adjacent to the player
            if (route[route.Count - 1] == MainCharacter.CurrentPosition) route.RemoveAt(route.Count - 1);

            mOrcs[i].CurrentPath = route;
            mOrcs[i].PlayVoice(Character.VoiceTypes.MOVE);
            mOrcs[i].Run();

            while (mOrcs[i].IsRunning)
            {
                yield return null;
            }

            //Make the position not accessible so that other orcs don't try to use the space in the future
            mOrcs[i].CurrentPosition.IsAccessible = false;

            //if adjacent to the player then it's game over for them
            if (mOrcs[i].CurrentPosition.Connections.Contains(MainCharacter.CurrentPosition))
            {
                MainCharacter.transform.rotation = Quaternion.LookRotation(mOrcs[i].CurrentPosition.Position - MainCharacter.CurrentPosition.Position, Vector3.up);
                MainCharacter.Animator.Play(AnimatorCharacterDieToOrc);

                //Play the attack animation for the orc and the player's death animation at the same time
                mOrcs[i].PlayVoice(Character.VoiceTypes.ATTACK);
                mOrcs[i].transform.rotation = Quaternion.LookRotation(MainCharacter.CurrentPosition.Position - mOrcs[i].CurrentPosition.Position, Vector3.up);
                mOrcs[i].Animator.Play(AnimatorCharacterOrcAttack);
                while (MainCharacter.Animator.GetCurrentAnimatorStateInfo(0).shortNameHash != AnimatorCharacterDieToOrc)
                {
                    yield return null;
                }
                while (MainCharacter.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f)
                {
                    yield return null;
                }

                MainCharacter.PlayVoice(Character.VoiceTypes.DEATH);

                if (GameOverScreen != null)
                {
                    GameOverScreen.gameObject.SetActive(true);
                    GameOverScreen.enabled = true;
                    Hud.enabled = false;
                    SurvivalMessageTextMesh.text = $"You survived for [{TurnCount}] turns!";
                }
                else
                {
                    Debug.LogError("No GameOverScreen exists!");
                }
                
                yield break;
            }
        }
        #endregion

        Input.ResetInputAxes();
        yield return null;

        PlayPauseAnimator.SetBool(PlayPauseAnimatorPlaying, false);

        TurnCount += 1;
        mMap.ReduceTileSnow();
        if (TurnCount >= nextOrcTurn) // we should spawn orc(s)
        {
            nextOrcTurn = TurnCount + 1;
            orcsToSpawn = 1; //For gameplay reasons I felt it was better not to spawn more than 1 orc per turn
            for (int i = 0; i < orcsToSpawn; i++)
            {
                if (i == 0 && TurnCount % 10 == 0) SpawnOrc(FireOrc);
                else SpawnOrc(Orc);
            }
        }

        if (TurnCount % 50 == 0) mMap.MakeAccessibleTilesSnow();

        TurnCounterTextMesh.text = $"Turn [{TurnCount}]";

        ActionBarUI.CleanActions();

        canTakeTurn = true;
        ActionCounterTextMesh.text = $"Actions: 0/{MainCharacter.MaxActionPoints}";
    }

    void SpawnOrc(Character orcPrefab)
    {
        if (OrcSpawns == null || OrcSpawns.Count == 0) return; //there are no spots to spawn from

        //Calculate which spawn tiles aren't currently taken by other characters
        List<EnvironmentTile> openSpawns = new List<EnvironmentTile>(OrcSpawns);
        int i = 0;
        while (i < openSpawns.Count)
        {
            bool remove = false;
            if (MainCharacter.CurrentPosition == openSpawns[i])
            {
                remove = true;
            }
            else
            {
                for (int j = 0; j < mOrcs.Count; j++)
                {
                    if (mOrcs[j].CurrentPosition == openSpawns[i])
                    {
                        remove = true;
                        break;
                    }
                }
            }
            
            if (remove)
            {
                openSpawns.RemoveAt(i);
                continue;
            }

            i++;
        }

        if (openSpawns.Count == 0) return; //there are no open spots to spawn from

        EnvironmentTile spawnTile = openSpawns[Random.Range(0, openSpawns.Count)];

        Character newOrc = Instantiate(orcPrefab, transform);
        newOrc.transform.position = spawnTile.Position;
        newOrc.transform.rotation = Quaternion.identity;
        newOrc.CurrentPosition = spawnTile;

        //Give the orc its personalised voice
        newOrc.GetComponent<AudioSource>().pitch = Random.Range(1f, 2f);
        AudioClip personalOrcSound = OrcVoiceContainer.GetRandomClip();
        newOrc.DeathSounds = new AudioClip[] { personalOrcSound };
        newOrc.MoveSounds = new AudioClip[] { personalOrcSound };
        newOrc.AttackSounds = new AudioClip[] { personalOrcSound };

        mOrcs.Add(newOrc);
    }

    public void ShowMenu(bool show)
    {
        Instantiate(ButtonClickSound);

        if (Menu != null && Hud != null)
        {
            Menu.gameObject.SetActive(show);
            Menu.enabled = show;
            Hud.enabled = !show;

            if (GameOverScreen != null)
            {
                GameOverScreen.enabled = false;
                GameOverScreen.gameObject.SetActive(false);
            }

            if (HelpMenu != null)
            {
                HelpMenu.enabled = false;
                HelpMenu.gameObject.SetActive(false);
            }

            if (SettingsMenu != null)
            {
                SettingsMenu.enabled = false;
                SettingsMenu.gameObject.SetActive(false);
            }

            if ( show )
            {
                MainCharacter.transform.position = CharacterStart.position;
                MainCharacter.transform.rotation = CharacterStart.rotation;
                mMap.CleanUpWorld();
                ResetVariables();
            }
            else
            {
                MainCharacter.transform.position = mMap.Start.Position;
                MainCharacter.transform.rotation = Quaternion.identity;
                MainCharacter.CurrentPosition = mMap.Start;
            }
        }
    }

    public void Generate()
    {
        mMap.GenerateWorld();
    }

    public void ShowHelp()
    {
        Instantiate(ButtonClickSound);

        if (HelpMenu == null) return;
        HelpMenu.gameObject.SetActive(true);
        HelpMenu.enabled = true;

        Menu.gameObject.SetActive(false);
        Menu.enabled = false;
        Hud.enabled = false;
    }

    public void ShowSettings()
    {
        Instantiate(ButtonClickSound);

        if (SettingsMenu == null) return;
        SettingsMenu.gameObject.SetActive(true);
        SettingsMenu.enabled = true;

        Menu.gameObject.SetActive(false);
        Menu.enabled = false;
        Hud.enabled = false;
    }

    public void Exit()
    {
        Instantiate(ButtonClickSound);
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}
