﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    AudioSource audioSource;

    [SerializeField] AudioClip startTrack;
    [SerializeField] long startLoopPosition;
    [Space]
    [SerializeField] AudioClip loopTrack;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = startTrack;
        audioSource.Play();
        audioSource.loop = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (audioSource.loop == false)
        {
            //When we get to the start of the loop track in the non-looped track, switch to the looped track
            if (audioSource.timeSamples >= startLoopPosition)
            {
                audioSource.Stop();
                audioSource.clip = loopTrack;
                audioSource.Play();
                audioSource.loop = true;
                
                enabled = false;
            }
        }
    }
}
