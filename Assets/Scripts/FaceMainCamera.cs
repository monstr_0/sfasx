﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceMainCamera : MonoBehaviour
{
    void Update()
    {
        transform.LookAt(CameraControl.MainCamera.transform.position);
    }
}
