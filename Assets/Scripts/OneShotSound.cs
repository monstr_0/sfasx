﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotSound : MonoBehaviour
{
    public AudioSource AudioSource { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
        AudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (AudioSource.clip != null)
        {
            if (AudioSource.isPlaying == false) Destroy(gameObject);
        }
    }
}
