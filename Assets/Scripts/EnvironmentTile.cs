﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnvironmentTile : MonoBehaviour
{
    public List<EnvironmentTile> Connections { get; set; }
    public EnvironmentTile Parent { get; set; }
    public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }
    public bool IsAccessible { get; set; }
    public bool IsScenery { get; set; } //Is not accessible but can be destroyed by orcs if needed

    public int SnowTurns { get; set; }
    public bool IsSnow { get { return SnowTurns == 0; } }

    MeshRenderer meshRenderer;
    [SerializeField] MeshRenderer indicatorMeshRenderer;
    [SerializeField] Material indicatorHoverMaterial;
    [SerializeField] Material indicatorHoverUnavailableMaterial;
    [SerializeField] Material indicatorDestinationMaterial;
    [SerializeField] Material indicatorPathMaterial;
    [Space]
    [SerializeField] int groundMaterialIndex = 1;
    [SerializeField] Material snowGroundMaterial;
    [SerializeField] Material noSnowGroundMaterial;
    [Space]
    [SerializeField] Canvas snowTurnsCanvas;
    [SerializeField] TextMeshProUGUI snowTurnsTextMesh;
    [Space]
    public Transform sceneryForClearing;

    public bool IsPath { get; set; }
    public bool IsHovering { get; set; }
    public bool IsDestination { get; set; }

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        indicatorMeshRenderer.enabled = false;
    }

    private void Update()
    {
        SetIndicator();
        SetGroundSnow();
        SetSnowTurnCanvas();
    }

    public IEnumerator ClearOfScenery()
    {
        if (IsScenery && sceneryForClearing != null)
        {
            //Reduce size to nothing then delete

            while (sceneryForClearing.localScale.x > 0.1f)
            {
                sceneryForClearing.localScale = Vector3.Lerp(sceneryForClearing.localScale, Vector3.zero, Time.deltaTime * Character.MovementMultiplier);
                yield return null;
            }

            Destroy(sceneryForClearing.gameObject);

            IsScenery = false;
            IsAccessible = true;
            sceneryForClearing = null;
        }
    }

    void SetIndicator()
    {
        bool showRenderer = true;

        if (IsHovering) indicatorMeshRenderer.sharedMaterial = IsAccessible ? indicatorHoverMaterial : indicatorHoverUnavailableMaterial;
        else if (IsDestination) indicatorMeshRenderer.sharedMaterial = indicatorDestinationMaterial;
        else if (IsPath) indicatorMeshRenderer.sharedMaterial = indicatorPathMaterial;
        else showRenderer = false;

        indicatorMeshRenderer.enabled = showRenderer;
    }

    void SetGroundSnow()
    {
        Material[] materialArray = meshRenderer.sharedMaterials;
        materialArray[groundMaterialIndex] = IsSnow ? snowGroundMaterial : noSnowGroundMaterial;
        meshRenderer.sharedMaterials = materialArray;
    }

    void SetSnowTurnCanvas()
    {
        if (snowTurnsCanvas)
        {
            if (SnowTurns > 0 && IsHovering)
            {
                snowTurnsCanvas.gameObject.SetActive(true);
                snowTurnsTextMesh.text = SnowTurns.ToString();
            }
            else
            {
                snowTurnsCanvas.gameObject.SetActive(false);
            }
        }
    }
}
