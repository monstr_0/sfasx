﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionBarUI : MonoBehaviour
{
    static ActionBarUI instance;

    [SerializeField] Sprite movementSprite;
    [SerializeField] Sprite attackSprite;
    [SerializeField] Sprite skipSprite;

    public enum ActionTypes
    {
        NONE,
        MOVEMENT,
        ATTACK,
        SKIP
    }

    [SerializeField] ActionTypes[] _actions;
    public static ActionTypes[] Actions { get; private set; }

    [SerializeField] ActionUI[] actionUIs;
    static ActionUI[] ActionUIs { get { return instance?.actionUIs; } }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        CleanActions();
    }

    private void Update()
    {
        _actions = Actions;
    }

    public static void UpdateActions()
    {
        if (instance == null) return;

        for (int i = 0; i < Actions.Length; i++)
        {
            if (ActionUIs.Length <= i) break;
            if (ActionUIs[i] == null) continue;
            switch (Actions[i])
            {
                case ActionTypes.ATTACK:
                    ActionUIs[i].color = Color.red;
                    ActionUIs[i].sprite = instance.attackSprite;
                    ActionUIs[i].show = true;
                    break;
                case ActionTypes.MOVEMENT:
                    ActionUIs[i].color = Color.green;
                    ActionUIs[i].sprite = instance.movementSprite;
                    ActionUIs[i].show = true;
                    break;
                case ActionTypes.SKIP:
                    ActionUIs[i].color = Color.white;
                    ActionUIs[i].sprite = instance.skipSprite;
                    ActionUIs[i].show = true;
                    break;
                default:
                    ActionUIs[i].color = Color.clear;
                    ActionUIs[i].isLast = true;
                    ActionUIs[i].sprite = null;
                    ActionUIs[i].show = false;
                    if (i > 0) ActionUIs[i - 1].isLast = true;
                    break;
            }

            if (i == Actions.Length - 1) ActionUIs[i].isLast = true;
        }
    }

    public static void CleanActions()
    {
        if (instance == null) return;

        Actions = new ActionTypes[instance.actionUIs.Length];
        for (int i = 0; i < Actions.Length; i++)
        {
            Actions[i] = ActionTypes.NONE;
        }

        UpdateActions();
    }
}
